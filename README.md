# civitas-bd


## Modelo do banco de dados

![CivitasBD.png](./CivitasBD.png)


## Usage
Dentro da pasta do projeto, rode o comando "npm install" para instalar as dependências e depois "npm run dev" para iniciar o servidor.
Utilize o postman para testar a CRUD.


## Criar usuário
Fisica (/fisica):
    nome, email, senha, cpf, dataNascimento

Jurídica (/juridica):

    nome, email, senha, cnpj, cep, complemento, numero, nomeRepresentante

Item (/item):
    titulo, descricao, preco,quantidade, categoria, idPessoa


## Editar 
(O id refere-se ao id do usuário)
Fisica (/fisica/update/:id)
Jurídica (/juridica/update/:id)

## Deletar
Item (/item/delete)
Fisica (/item/delete/:id)
Juridica (/juridica/delete/:id)

## Buscar
User (/user/:id)
Item (/item/:titulo/:categoria)
Item (/item/:id)
