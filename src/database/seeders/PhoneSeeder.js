const Phone = require("../../models/Phone");
const faker = require('faker-br');

const seedPhone = async function(){
    const phones = [];

    for(let i=0; i< 10; i++){
        phones.push({
            model_name: faker.commerce.product(),
            manufacturer: faker.company.companyName(),
            date_of_acquire: faker.date.past(),
            has_nfc: faker.random.boolean(),
            chips_number: faker.random.number({
                'min': 1,
                'max': 3
            }),
        });
    }

    try {
        await Phone.sync({ force: true});
        await Phone.bulkCreate(phones);
    } catch (err) {
        console.log(err);
    }
}

module.exports = seedPhone;