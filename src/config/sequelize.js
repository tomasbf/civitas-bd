const { Sequelize } = require("sequelize");
const setRelacoes = require("../models/relacoes");

const sequelize = (process.env.DB_CONNECTION === 'sqlite')?
  new Sequelize({
    dialect: 'sqlite',
    storage: process.env.DB_HOST + process.env.DB_DATABASE
  })
  :
  new Sequelize(
    process.env.DB_DATABASE,
    process.env.DB_USERNAME,
    process.env.DB_PASSWORD,
    {
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        dialect: process.env.DB_CONNECTION,
        models: [__dirname + "/../models"]
    }

  );


const Item = sequelize.define('Item', require('../models/Item.js'));
const User = sequelize.define('User', require('../models/User.js'));
const PessoaFisica = sequelize.define('PessoaFisica', require('../models/PessoaFisica.js'));
const PessoaJuridica = sequelize.define('PessoaJuridica', require('../models/PessoaJuridica.js'));




setRelacoes(PessoaFisica, PessoaJuridica, User, Item);

for (mod in sequelize.models) {
  if (sequelize.models[mod].associate instanceof Function) {
    sequelize.models[mod].associate(sequelize.models);
  }
}

module.exports = {
  sequelize,
  Item,
  User,
  PessoaFisica,
  PessoaJuridica
};
    