const { body } = require("express-validator");

const validationItem = (method) =>{
    switch(method){
        case 'create': {
            return [
                body('titulo').exists().withMessage("This Field mustn't be null").isLength({min: 1}).withMessage('Insira o título'),
                body('descricao').exists().withMessage("This Field mustn't be null").isLength({min: 1}).withMessage('Insira a descrição'),
                body('preco').exists().withMessage("This Field mustn't be null").isLength({min: 1}).withMessage('Insira o preço'),
                body('categoria').exists().withMessage("This Field mustn't be null")
            ]
        }
    }
};
const validationFisica = (method) =>{

    switch(method){
        case 'create': {
            return [
                body('nome').exists().withMessage("This Field mustn't be null").isLength({min: 1}).withMessage('Digite seu nome'),
                body('cpf').exists().withMessage("This Field mustn't be null").isLength({min: 11}).withMessage('Insira o CPF'),
                body('dataNascimento').exists().withMessage("This Field mustn't be null").isLength({min: 1}).withMessage('Coloque a sua data de nascimento').isDate().withMessage('Assim: DD/MM/YYYY'),
                body('email').exists().withMessage("This Field mustn't be null").isLength({min: 1}).withMessage('Insira o seu email').isEmail().withMessage('Precisa ser exemplo@exemplo'),
                body('senha').exists().withMessage("This Field mustn't be null").isLength({min: 1}).withMessage('Insira a sua senha').isLength({min: 8}).withMessage('A senha deve ter no mínimo 8 caracteres')
            ]
        }
    }   
};

const validationJuridica = (method) =>{
    switch(method){
        case 'create': {
            return [
                body('email').exists().withMessage("This Field mustn't be null").isLength({min: 1}).withMessage('Insira o email de contato').isEmail().withMessage('Precisa ser exemplo@exemplo'),
                body('nome').exists().withMessage("This Field mustn't be null").isLength({min: 1}).withMessage('Digite o nome do representante'),
                body('cnpj').exists().withMessage("This Field mustn't be null").isLength({min: 14}).withMessage('Insira o CNPJ'),
                body('cep').exists().withMessage("This Field mustn't be null").isLength({min: 8}).withMessage('Insira o CEP'),
                body('complemento').exists().withMessage("This Field mustn't be null").isLength({min: 1}).withMessage('Insira o complemento do endereço'),
                body('numero').exists().withMessage("This Field mustn't be null").isLength({min: 1}).withMessage('Insira o número do prédio'),
                body('senha').exists().withMessage("This Field mustn't be null").isLength({min: 1}).withMessage('Insira a sua senha').isLength({min: 8}).withMessage('A senha deve ter no mínimo 8 caracteres')
            ]
        }
    }
};

const validationLogar = () =>{

    return [
        body('email').exists().withMessage("This Field mustn't be null").isLength({min: 1}).withMessage('Insira o email').isEmail().withMessage('Precisa ser exemplo@exemplo'),
        body('senha').exists().withMessage("This Field mustn't be null").isLength({min: 1}).withMessage('Insira a sua senha').isLength({min: 8}).withMessage('A senha deve ter no mínimo 8 caracteres')
    ]

};


module.exports = {
    validationFisica,
    validationItem,
    validationJuridica,
    validationLogar
}