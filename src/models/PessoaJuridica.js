const { DataTypes } = require("sequelize");

const PessoaJuridica = {
    id: {

        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true

    },
    nome: {

        type: DataTypes.STRING,
        allowNull: false

    },

    cnpj: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    },
    cep: {

        type: DataTypes.STRING,
        allowNull: false
    },
    complemento: {

        type: DataTypes.STRING,
        allowNull: false
    },
    numero: {

        type: DataTypes.INTEGER,
        allowNull: false
    },
    nomeRepresentante: {

        type: DataTypes.STRING,
        allowNull: false

    },
    userId: {
        type: DataTypes.INTEGER,
        foreignKey: true,
        unique: true,
        allowNull: true
    }
};

module.exports = PessoaJuridica;
