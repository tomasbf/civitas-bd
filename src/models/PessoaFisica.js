
const { DataTypes } = require("sequelize");



const PessoaFisica = {
    id: {

        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true

    },
    nome: {

        type: DataTypes.STRING,
        allowNull: false

    },

    cpf: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    },
    dataNascimento: {

        type: DataTypes.DATE,
        allowNull: false
    },
    userId: {
        type: DataTypes.INTEGER,
        unique: true,
        foreignKey: true,
        allowNull: true
    }

};

module.exports = PessoaFisica;
