const { DataTypes } = require("sequelize");


const Item ={

    id: {

        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    titulo: {

        type: DataTypes.STRING,
        allowNull: false

    },
    descricao: {

        type: DataTypes.STRING,
        allowNull: false

    },
    preco: {

        type: DataTypes.FLOAT,
        allowNull: false
    },
    quantidade: {

        type: DataTypes.INTEGER,
        allowNull: false
    },
    categoria: {

        type: DataTypes.STRING,
        allowNull: false
    },
    idPessoa: {

        type: DataTypes.INTEGER,
        foreignKey: true,
        allowNull: false

    }


};
module.exports = Item;

