const setRelacoes = (PessoaFisica, PessoaJuridica, User, Item) => {

    PessoaFisica.belongsTo(User, { foreignKey: 'userId' });
    PessoaJuridica.belongsTo(User, { foreignKey: 'userId' });

    User.hasOne(PessoaFisica, { foreignKey: 'idPessoa' });
    User.hasOne(PessoaJuridica, { foreignKey: 'idPessoa' });

    Item.belongsTo(User, { foreignKey: 'idPessoa' });
    User.hasMany(Item);

}
module.exports = setRelacoes;



