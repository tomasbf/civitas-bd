const { DataTypes } = require("sequelize");

const User = {

    id: {

        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    email: {

        type: DataTypes.STRING,
        allowNull: false,
        unique: true

    },
    tipo: {
        type: DataTypes.STRING,
        allowNull: true
    },
    
    hash: {

        type: DataTypes.STRING,
        allowNull: false

    }, 
    salt: {

        type: DataTypes.STRING,
        allowNull: false

    },
    idPessoa: {
        type: DataTypes.INTEGER,
        allowNull: false,
        foreignKey: true
    }


};

module.exports = User;