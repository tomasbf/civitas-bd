const router = require("express").Router();
const validator = require("../config/validator");
const FisicaController = require('../controllers/FisicaController');
const JuridicaController = require('../controllers/JuridicaController');
const ItemController = require('../controllers/ItemController');
const AuthController = require('../controllers/AuthController');

const setAuthorizationHeader = require('../middlewares/token');
const bodyParser = require('body-parser');
const urlEncodeParser = bodyParser.urlencoded({extended: false});
const passport = require('passport');


router.post('/fisica', validator.validationFisica('create'), FisicaController.create);
router.post('/user', validator.validationLogar(), AuthController.login);
router.post('/juridica', validator.validationJuridica('create'), JuridicaController.create);

router.get('/user/:id', AuthController.getById);

router.delete('/fisica/delete/:id', FisicaController.destroy);
router.delete('/juridica/delete/:id', JuridicaController.destroy);


router.put('/fisica/update/:id', FisicaController.update);
router.put('/juridica/update/:id', JuridicaController.update);


router.post('/item', validator.validationItem('create'), ItemController.create);

router.delete('/item/delete', ItemController.destroy);

router.get('/item/:titulo/:categoria', ItemController.search);
router.get('/item/:id', ItemController.show);


module.exports = router;