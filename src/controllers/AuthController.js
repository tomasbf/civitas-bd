const Auth = require('../config/auth');
const FisicaController = require('../controllers/FisicaController');
const JuridicaController = require('../controllers/JuridicaController');
const { User } = require('../config/sequelize');


const register = async(req,res) => {

    try {
        const generateHash = Auth.generateHash(req.body.senha);
        const salt = generateHash.salt;
        const hash = generateHash.hash;


        const newUserData = {
            email: req.body.email,
            tipo: req.body.tipo,
            hash: hash,
            salt: salt
        }

        const user = await User.create(newUserData);
        
        
        return await res.status(201).json( { user } );

    }
    catch (err) {
        return res.status(500).json({ error: err });
    }

};

const login = async (req, res) => {
    try {
        const user = await User.findOne({ where: { email: req.body.email } });
        if (!user) { throw new Error(); }
        const isValid = Auth.verifyPassword(req.body.senha, user.dataValues.salt, user.dataValues.hash);
        if (isValid) {
            const token = Auth.generateJsonWebToken(user);
            res.status(200).json({
                message: "Usuário logado com sucesso.",
                token: token
            });
        }
        else {
            res.status(401).json({ message: "Voce entrou com a senha incorreta." });
        }
    } catch (err) {
        res.status(401).json({ message: "Esse usuário não existe." });
    }
};

const getById = async (req, res) => {
    try {
        
        const { id } = req.params;
        const result = await User.findByPk(id);
        const idPessoa = await result.idPessoa;

        switch(result.tipo) {
            case 'fisica':

                const fisica = await PessoaFisica.findByPk(idPessoa);

                let newData = {

                    id: result.id,
                    email: result.email,
                    nome: await fisica.nome,
                    dataNascimento: await fisica.dataNascimento,
                    cpf: await fisica.cpf

                };

                return res.status(200).json({ content: newData });

            case 'juridica':

                const juridica = await PessoaJuridica.findByPk(idPessoa);

                newData = {

                    id: result.id,
                    nome: await juridica.nome,
                    email: result.email,
                    cnpj: await juridica.cnpj,
                    cep: await juridica.cep,
                    complemento: await juridica.complemento,
                    numero: await juridica.numero,
                    nomeRepresentante: await juridica.nomeRepresentante

                };
                return res.status(200).json({ content: newData });

            default:
                return res.status(500).json({ error: 'Tipo de usuário não encontrado' });

        }


    } catch (error) {
        
        return res.status(500).json({ error });

    }
};

const getDetails = (req, res) => {
	const token = Auth.getToken(req);
	const loggedUser = Auth.user(token);
	return res.status(201).json({ user: loggedUser});
};

const search = async (req, res) => {
    try {
        
        const { email } = req.body;
        const result = await User.findAll({
            where: {
                [Op.and]: {
                    email: {
                        [Op.like]: '%' + email + '%'
                    },
                    tipo: {
                        [Op.like]: '%' + req.body.tipo + '%'
                    }
                }
            }
        });
        
        let content = [];
        for(let i = 0; i < result.length; i++){
            idPessoa = await result[i].idPessoa;

            switch(tipo) {
                case 'fisica':

                    const fisica = await PessoaFisica.findByPk(idPessoa);

                    let newData = {

                        id: result[i].id,
                        email: result[i].email,
                        nome: await fisica.nome,
                        dataNascimento: await fisica.dataNascimento,
                        cpf: await fisica.cpf

                    };

                    content.push(newData);
                    break;

                case 'juridica':

                    const juridica = await PessoaJuridica.findByPk(idPessoa);

                    newData = {

                        id: result[i].id,
                        nome: await juridica.nome,
                        email: result[i].email,
                        cnpj: await juridica.cnpj,
                        cep: await juridica.cep,
                        complemento: await juridica.complemento,
                        numero: await juridica.numero,
                        nomeRepresentante: await juridica.nomeRepresentante

                    };
                    content.push(await PessoaJuridica.findByPk(idPessoa));

                    break;

                default:
                    return res.status(500).json({ error: 'Tipo de usuário não encontrado' });

            }


        }

        return res.status(200).json({ content });

    } catch (error) {
        res.status(500).json({ error });
    }
        
};



module.exports = {
    register,
    login,
    getById,
    getDetails,
    search
}