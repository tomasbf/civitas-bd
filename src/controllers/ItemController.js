const { Item } = require('../config/sequelize');
const {validationItem} = require('../config/validator');
const {Op} = require('sequelize');

const index = async(req, res) =>{
    try{
        const item = await Item.findAll();
        return res.status(200).json({item});
    } catch(err){
        return res.status(500).json({err});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const item = await Item.findByPk(id);
        return res.status(200).json({item});
    }catch(err){
        return res.status(500).json({err});
    }
};


const search = async(req, res) =>{
    const {titulo, categoria} = req.body;
    try{
        const result = await Item.findAll({
            where:{
                [Op.and]:{
                    titulo: {
                        [Op.like]: '%'+titulo+'%'
                    },
                    categoria: {
                        [Op.like]: categoria
                    }
                }
            }
        });
        return res.status(200).json({result});
    }catch{
        res.status(500).json({error: err});
    }
}

const create = async(req,res) => {
    try{
          validationItem(req).throw(); //validação
          const item = await Item.create(req.body);
          console.log(req.body);
          return res.status(201).json({message: `${item.categoria} cadastrado com sucesso!`, item: item});
      } catch(err){
          res.status(500).json({error: err});
      }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Item.update(req.body, {where: {id: id}});
        if(updated) {
            const item = await Item.findByPk(id);
            return res.status(200).send(item);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Item não encontrado");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Item.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Item deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Item não encontrado.");
    }
};

module.exports = {
    update,
    destroy,
    create,
    index,
    show,
    search
}
