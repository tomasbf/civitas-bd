const { PessoaJuridica } = require('../config/sequelize');
const { validationJuridica } = require('../config/validator');
const { Op } = require('sequelize');
const { Auth } = require('../config/auth');
const { AuthController } = require('../controllers/AuthController');
const { User } = require('../config/sequelize');



const index = async (req, res) => {
    try {
        const user = await PessoaJuridica.findAll();
        return res.status(200).json({ user });
    } catch (error) {

        return res.status(500).json({ error });

    }

}

const show = async (req, res) => {
    try {
        const user = await Juridica.findByPk(req.params.id);
        return res.status(200).json({ user });
    } catch (error) {
        return res.status(500).json({ error });
    }
}


const search = async (req, res) => {
    const { email } = req.body;
    try {
        const result = await PessoaJuridica.findAll({
            where: {
                [Op.and]: {
                    email: {
                        [Op.like]: '%' + email + '%'
                    }
                }
            }
        });
        return res.status(200).json({ result });
    } catch {
        res.status(500).json({ error: err });
    }
}

const create = async (req, res) => {
    try {
        validationJuridica(req).throw(); //validação
        const newUserInfo = {
            email: req.body.email,
            tipo: "juridica",
            senha: req.body.senha,
        };
        const newUserData = {
            nome: req.body.nome,
            cnpj: req.body.cnpj,
            cep: req.body.cep,
            complemento: req.body.complemento,
            numero: req.body.numero
    
        };


        const userInfo = await AuthController.register(newUserInfo);
        const user = await PessoaJuridica.create(newUserData);

        await User.update({ idPessoa: user.id }, { where: { id: userInfo.id } });
        await PessoaJuridica.update({ userId: userInfo.id }, { where: { id: user.id } });

        console.log(req.body);
        return res.status(201).json({ message: "Usuário cadastrado com sucesso!", user: user });
    } catch (err) {
        res.status(500).json({ error: err });
    }
};

const update = async (req, res) => {
    const { id } = req.params;
    const newData = {
        email: req.body.email
    };
    try {
        const [updated] = await PessoaJuridica.update(req.body, { where: { idPessoa: id } });
        const [updatedUser] = await User.update(newData, { where: { id: id } });
        if (updated) {
            const user = await PessoaJuridica.findByPk(id);
            return res.status(200).send(user);
        }
        throw new Error();
    } catch (err) {
        return res.status(500).json("Usuário não encontrado");
    }
};

const destroy = async (req, res) => {
    const { id } = req.params;
    try {
        const juridica = await PessoaJuridica.findOne({where: {id: id}});
        if(await juridica && await User.findByPk(juridica.userId)){
            const deleted = await PessoaJuridica.destroy({ where: { idPessoa: id } });
            const deletedUser = await User.destroy({ where: { id: id } });
            if (deleted && deletedUser) {
                return res.status(200).json("Usuário deletado com sucesso.");
            }
        }
        throw new Error();
    } catch (err) {
        return res.status(500).json("Usuário não encontrado.");
    }
};


module.exports = {
    update,
    destroy,
    create,
    index,
    show,
    search
}

