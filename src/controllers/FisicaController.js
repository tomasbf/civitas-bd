const { PessoaFisica } = require('../config/sequelize');
const {validationFisica} = require('../config/validator');
const { Op } = require('sequelize');
const {Auth} = require('../config/auth');
const AuthController = require('../controllers/AuthController');
const { User } = require('../config/validator');



const index = async (req, res) => {
    try {
        const fisicas = await PessoaFisica.findAll();
        return res.status(200).json({ fisicas });
    } catch (error) {

        return res.status(500).json({ error });

    }

}

const show = async (req, res) => {
    try {
        const fisica = await PessoaFisica.findByPk(req.params.id);
        return res.status(200).json({ fisica });
    } catch (error) {
        return res.status(500).json({ error });
    }
}


const search = async (req, res) => {
    const { email } = req.body;
    try {
        const result = await PessoaFisica.findAll({
            where: {
                [Op.and]: {
                    email: {
                        [Op.like]: '%' + email + '%'
                    }
                }
            }
        });
        return res.status(200).json({ result });
    } catch {
        res.status(500).json({ error: err });
    }
}

const create = async (req, res) => {

    try {
        validationFisica(req).throw(); 

        const newUserInfo = {
            email: req.body.email,
            tipo: "fisica",
            senha: req.body.senha,
        };
        const newUserData = {
            nome: req.body.nome,
            dataNascimento: req.body.dataNascimento,
            cpf: req.body.cpf

        }
        const userInfo = await AuthController.register(newUserInfo);
        const user = await PessoaFisica.create(newUserData);


        await User.update({ idPessoa: user.id }, { where: { id: userInfo.id } });
        await PessoaFisica.update({ userId: userInfo.id }, { where: { id: user.id } });
        
        console.log(req.body);

        return res.status(201).json({ message: "Usuário cadastrado com sucesso!", user: user });
    } catch (err) {
        return res.status(500).json({ error: "Dados inválidos" });
    }
};

const update = async (req, res) => {
    const { id } = req.params;
    const newData = {
        email: req.body.email
    };
    try {
        const [updated] = await PessoaFisica.update(req.body, { where: { idPessoa: id } });
        const [updatedUser] = await User.update(newData, { where: { id: id } });

        if (updated) {
            const user = await PessoaFisica.findByPk(id);
            return res.status(200).send(user);
        }
        throw new Error();
    } catch (err) {
        return res.status(500).json("Usuário não encontrado");
    }
};

const destroy = async (req, res) => {
    const { id } = req.params;
    try {
        const fisica = await PessoaFisica.findOne({where: {idPessoa: id}});
        if(await fisica && await User.findByPk(id)){

            const deleted = await PessoaFisica.destroy({ where: { idPessoa: id } });
            const deletedUser = await User.destroy({ where: { id: id} });
            if (deleted && deletedUser) {
                return res.status(200).json("Usuário deletado com sucesso.");
            }
        }
        throw new Error();
    } catch (err) {
        return res.status(500).json("Usuário não encontrado.");
    }
};



module.exports = {
    update,
    destroy,
    create,
    index,
    show,
    search
}

